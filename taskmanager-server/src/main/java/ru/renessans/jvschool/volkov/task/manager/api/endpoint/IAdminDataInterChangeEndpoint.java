package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@SuppressWarnings("unused")
public interface IAdminDataInterChangeEndpoint extends IEndpoint {

    @WebMethod
    @WebResult(name = "clearedBinData", partName = "clearedBinData")
    boolean dataBinClear(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "clearedBase64Data", partName = "clearedBase64Data")
    boolean dataBase64Clear(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "clearedJsonData", partName = "clearedJsonData")
    boolean dataJsonClear(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "clearedXmlData", partName = "clearedXmlData")
    boolean dataXmlClear(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "clearedYamlData", partName = "clearedYamlData")
    boolean dataYamlClear(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataBin(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataBase64(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataJson(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataXml(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO exportDataYaml(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataBin(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataBase64(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataJson(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataXml(@Nullable SessionDTO sessionDTO);

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    DomainDTO importDataYaml(@Nullable SessionDTO sessionDTO);

}