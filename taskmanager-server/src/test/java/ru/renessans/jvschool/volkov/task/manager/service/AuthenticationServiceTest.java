package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class AuthenticationServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

    @NotNull
    private static IUserRepository USER_REPOSITORY;

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(USER_REPOSITORY);

    @NotNull
    private static final IAuthenticationService AUTH_SERVICE = new AuthenticationService(USER_SERVICE);

    @BeforeClass
    public static void preparingConfigurationBefore() {
        Assert.assertNotNull(PROPERTY_SERVICE);
        Assert.assertNotNull(CONFIG_SERVICE);
        PROPERTY_SERVICE.load();
    }

    @BeforeClass
    public static void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(USER_SERVICE);
        Assert.assertNotNull(AUTH_SERVICE);
    }

    @Test
    @TestCaseName("Run testNegativeVerifyValidUserData for verifyValidUserData({0}, {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeVerifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        @NotNull final InvalidLoginException loginThrown = assertThrows(
                InvalidLoginException.class,
                () -> AUTH_SERVICE.verifyValidUserData(login, password)
        );
        Assert.assertNotNull(loginThrown);
        Assert.assertNotNull(loginThrown.getMessage());

        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
                InvalidPasswordException.class,
                () -> AUTH_SERVICE.verifyValidUserData(tempLogin, password)
        );
        Assert.assertNotNull(passwordThrown);
        Assert.assertNotNull(passwordThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeSignUp for signUp(\"{0}\", \"{1}\", \"{3}\", {4})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersAllFieldsCaseData"
    )
    public void testNegativeSignUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName,
            @Nullable final UserRoleType userRoleType
    ) {
        @NotNull final InvalidLoginException loginThrown = assertThrows(
                InvalidLoginException.class,
                () -> AUTH_SERVICE.signUp(login, password)
        );
        Assert.assertNotNull(loginThrown);
        Assert.assertNotNull(loginThrown.getMessage());

        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
                InvalidPasswordException.class,
                () -> AUTH_SERVICE.signUp(tempLogin, password)
        );
        Assert.assertNotNull(passwordThrown);
        Assert.assertNotNull(passwordThrown.getMessage());

        @NotNull final String tempPassword = DemoDataConst.USER_DEFAULT_PASSWORD;
        @NotNull final InvalidFirstNameException firstNameThrown = assertThrows(
                InvalidFirstNameException.class,
                () -> AUTH_SERVICE.signUp(tempLogin, tempPassword, firstName)
        );
        Assert.assertNotNull(firstNameThrown);
        Assert.assertNotNull(firstNameThrown.getMessage());

        @NotNull final InvalidUserRoleException roleThrown = assertThrows(
                InvalidUserRoleException.class,
                () -> AUTH_SERVICE.signUp(tempLogin, tempPassword, userRoleType)
        );
        Assert.assertNotNull(roleThrown);
        Assert.assertNotNull(roleThrown.getMessage());
    }

//    @Test
//    @TestCaseName("Run testVerifyValidPermission for verifyValidPermission(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidPermission(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = AUTH_SERVICE.signUp(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final PermissionValidState permissionValidState =
//                AUTH_SERVICE.verifyValidPermission(addUser.getId(), addUser.getRole());
//        Assert.assertNotNull(permissionValidState);
//        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
//        @NotNull final User clearUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(clearUser);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = AUTH_SERVICE.signUp(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final UserDataValidState userDataValidState = AUTH_SERVICE.verifyValidUserData(login, password);
//        Assert.assertNotNull(userDataValidState);
//        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
//        @NotNull final User clearUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(clearUser);
//    }
//
//    @Test
//    @TestCaseName("Run testSignUp for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testSignUp(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//
//        @NotNull final User addUser = AUTH_SERVICE.signUp(login, password);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(addUser.getId(), addUser.getId());
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(addUser.getRole(), addUser.getRole());
//        @NotNull final User clearUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(clearUser);
//    }
//
//    @Test
//    @TestCaseName("Run testSignUpWithUserRole for verifyValidUserData(\"{0}\", \"{1}\", {2})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithRoleCaseData"
//    )
//    public void testSignUpWithUserRole(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final UserRole userRole
//    ) {
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//
//        @NotNull final User addUser = USER_SERVICE.addUser(login, password, userRole);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(userRole, addUser.getRole());
//        @NotNull final User clearUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(clearUser);
//    }

}