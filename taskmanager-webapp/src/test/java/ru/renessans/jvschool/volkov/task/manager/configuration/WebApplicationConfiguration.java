package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.springframework.context.annotation.Import;

@Import({
        ApplicationConfiguration.class,
        WebMvcConfiguration.class,
        WebSecurityConfiguration.class,
        WebRestConfiguration.class
})
public class WebApplicationConfiguration {
}