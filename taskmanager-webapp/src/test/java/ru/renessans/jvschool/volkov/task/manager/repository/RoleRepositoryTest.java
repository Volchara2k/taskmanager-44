package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.configuration.DataSourceConfiguration;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.model.UserRole;

import java.util.Collections;
import java.util.HashSet;

@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfiguration.class)
@Category({PositiveImplementation.class, RepositoryImplementation.class})
public class RoleRepositoryTest {

    @Autowired
    private IUserRoleRepository roleRepository;

    @Autowired
    private IUserRepository userRepository;

    @Test
    public void testAdd() {
        @NotNull final User user = CaseDataUserProvider.createUser();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        @NotNull final User saveUserResponse = this.userRepository.save(user);
        Assert.assertNotNull(saveUserResponse);
        @NotNull final UserRole userRole = new UserRole(saveUserResponse, UserRoleType.USER);
        Assert.assertNotNull(userRole);
        Assert.assertNotNull(userRole.getId());
        saveUserResponse.setRoles(new HashSet<>(Collections.singleton(userRole)));

        @NotNull final UserRole addRecordResponse = this.roleRepository.save(userRole);
        Assert.assertNotNull(addRecordResponse);
        Assert.assertEquals(userRole.getUserRole(), addRecordResponse.getUserRole());
    }

}