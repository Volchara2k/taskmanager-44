package ru.renessans.jvschool.volkov.task.manager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.renessans.jvschool.volkov.task.manager.controller.view.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import java.util.Date;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Category({PositiveImplementation.class, EndpointImplementation.class})
public class TaskRestEndpointTest extends AbstractWebApplicationTest {

    @NotNull
    private final String baseUrl = "/api/tasks";

    @Test
    public void createTest() throws Exception {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setProjectId(PROJECT_1ST.getId());
        taskDTO.setId(UUID.randomUUID().toString());
        taskDTO.setUserId(USER.getId());
        taskDTO.setStatus(UserOwnerStatus.NOT_STARTED);
        TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        timeFrameDTO.setCreationDate(new Date());
        taskDTO.setTimeFrame(timeFrameDTO);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(taskDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/task/create")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void updateByIdTest() throws Exception {
        @NotNull final String title = "title";
        @NotNull final String description = "description";
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(TASK_1ST.getId());
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setUserId(USER.getId());

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        @NotNull final String requestJson = writer.writeValueAsString(taskDTO);

        assert TASK_1ST.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/task/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(TASK_1ST.getId()))
                .andExpect(jsonPath("$.title").value(title))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void getByIdTest() throws Exception {
        assert TASK_1ST.getId() != null;
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/task/view/{id}", TASK_1ST.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(TASK_1ST.getId()));
    }

    @Test
    public void tasksTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/task/delete/{id}", TASK_2ND.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

}