<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:input type="hidden" path="id" />
<form:input type="hidden" path="userId" />

<div class="write-content">

    <div class="form-group row">
        <label for="Title" class="col-sm-2 col-form-label">Заголовок:</label>
        <div class="col-sm-7">
            <form:input type="text" class="form-control" name="Title" path="title" placeholder="Введите заголовок" />
        </div>
    </div>

    <div class="form-group row">
        <label for="Description" class="col-sm-2 col-form-label">Описание:</label>
        <div class="col-sm-7">
            <form:input type="text" class="form-control" name="Description" path="description" placeholder="Введите описание" />
        </div>
    </div>

    <div class="form-group row">
        <label for="Status" class="col-sm-2 col-form-label">Статус:</label>
        <div class="col-sm-7">
            <form:select class="form-control" name="Status" path="status">
                <form:options items="${statuses}" itemLabel="title" />
            </form:select>
        </div>
    </div>

    <div class="form-group row">
        <label for="StartDate" class="col-sm-2 col-form-label">Дата начала:</label>
        <div class="col-sm-7">
            <form:input type="date" class="form-control" name="StartDate" path="timeFrame.startDate" />
        </div>
    </div>

    <div class="form-group row">
        <label for="EndDate" class="col-sm-2 col-form-label">Дата окончания:</label>
        <div class="col-sm-7">
            <form:input type="date" class="form-control" name="EndDate" path="timeFrame.endDate" />
        </div>
    </div>

    <div class="row justify-content-center">
        <button type="submit" class="btn btn-primary">Отправить</button>
    </div>

</div>