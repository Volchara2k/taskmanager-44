package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum UserOwnerStatus {

    @NotNull
    NOT_STARTED("Не начато"),

    @NotNull
    IN_PROGRESS("В прогрессе"),

    @NotNull
    COMPLETED("Завершено");

    @NotNull
    private final String title;

    UserOwnerStatus(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

}