package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ICookieService {

    @NotNull
    List<String> getCookieHeaders();

    void clearCookieHeaders();

    void saveCookieHeaders(@Nullable Object port);

    void setListCookieRowRequest(@Nullable Object port);

}